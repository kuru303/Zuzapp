import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import 'rxjs/add/observable/interval';

@IonicPage({
  name: 'visualizar'
})
@Component({
  selector: 'page-visualizar',
  templateUrl: 'visualizar.html',
})
export class VisualizarPage {

  img: any;

  constructor(
    private viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParam: NavParams,
    public sanitizer: DomSanitizer,
  ) {
    this.img = navParam.get('imagem');
  }

  fechar() {
    this.viewCtrl.dismiss();
  }

}
