import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/interval';
import { Observable } from 'rxjs/Observable';
import { AlertController, ModalController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  subs: any;
  refreshTime: number = 2000;
  urlBase: string = 'http://10.50.31.41:8100';
  capturas: any = [];
  iniciado: boolean = false;

  constructor(private http: HttpClient,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController) {
  }

  // soapCall() {
  //   let xmlhttp = new XMLHttpRequest();
  //   xmlhttp.open('POST', 'http://10.50.31.41:8100/soap/cgi-bin/server.php?wsdl', true);

  //   //the following variable contains my xml soap request (that you can get thanks to SoapUI for example)
  //   let sr =
  //     `<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lpr="lpr.stefano.benamati">
  //       <soapenv:Header/>
  //       <soapenv:Body>
  //          <lpr:getPlates soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/>
  //       </soapenv:Body>
  //    </soapenv:Envelope>`;

  //   xmlhttp.onreadystatechange = () => {
  //     console.log('xmlhttp ==>', xmlhttp);

  //     if (xmlhttp.readyState == 4) {
  //       if (xmlhttp.status == 200) {
  //         let xml = xmlhttp.responseXML;
  //         let retElement = xml.getElementsByTagName("ret")[0].childNodes[0];
  //         console.log( 'retElement', retElement );
  //         if ( retElement ) {
  //           let response_json = JSON.parse(retElement.nodeValue); //Here I'm getting the value contained by the <return> node
  //           console.log('response_json', response_json); //I'm printing my result square number
  //         }
  //       }
  //     }
  //   }
  //   // Send the POST request
  //   xmlhttp.setRequestHeader('Content-Type', 'text/xml');
  //   xmlhttp.responseType = "document";
  //   xmlhttp.send(sr);
  // }

  ionViewDidEnter() {
    this.iniciar();
  }

  ionViewDidLeave() {
    this.parar();
  }

  getImagem(placa, data) {
    let input =
      `<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lpr="lpr.stefano.benamati">
      <soapenv:Header/>
      <soapenv:Body>
         <lpr:getImage soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/>
      </soapenv:Body>
   </soapenv:Envelope>`;

    let headers: HttpHeaders = new HttpHeaders()
      .set('Content-Type', 'text/xml; charset=utf-8');

    this.http.post(`${this.urlBase}/soap/cgi-bin/server.php`, input, {headers, responseType: 'text' as 'text'})
      .subscribe( (res) => {
        // console.log(res);
        let retElement = new DOMParser().parseFromString(res, "text/xml").getElementsByTagName("ret")[0];
        // console.log('retElement', retElement.innerHTML);
        if ( retElement.innerHTML ) {
          // console.log('retElement.innerHTML', retElement.innerHTML); //I'm printing my result square number
          let fotoId = 'data:image/jpeg;base64,' + retElement.innerHTML;
          this.iniciar();
          this.capturas.unshift({placa: placa, data: data, foto: fotoId});
        }
      }, (error) => {
        console.log('error', error);
      });

  }

  getPlate() {
    let input =
      `<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lpr="lpr.stefano.benamati">
    <soapenv:Header/>
    <soapenv:Body>
       <lpr:getPlates soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/>
    </soapenv:Body>
    </soapenv:Envelope>`;

    let headers: HttpHeaders = new HttpHeaders()
      .set('Content-Type', 'text/xml; charset=utf-8');

    this.http.post(`${this.urlBase}/soap/cgi-bin/server.php`, input, {headers, responseType: 'text' as 'text'})
      .subscribe( (res) => {
        // console.log(res);
        let retElement = new DOMParser().parseFromString(res, "text/xml").getElementsByTagName("ret")[0];
        console.log('retElement Plate', retElement.innerHTML);
        if ( retElement.innerHTML ) {
          this.parar();
          let resp = '[' + retElement.innerHTML.replace(/[\r\n]/g, ',') + ']';
          console.log('resp =>', resp);
          
          let response_json = JSON.parse(resp); //Here I'm getting the value contained by the <return> node
          console.log('response_json', response_json); //I'm printing my result square number

          let reg = response_json[response_json.length - 1];
          let placa = reg.plate;
          let data = this.covertData(reg.timestamp);
          this.getImagem(placa, data);
        }
      }, (error) => {
        console.log('error', error);
      });

  }


  iniciar() {
    this.iniciado = true;
    this.refresh();
    if ( this.refreshTime ) {
      this.subs = Observable.interval(this.refreshTime).subscribe( () => { 
         this.refresh();
      });
    }
  }

  parar() {
    this.iniciado = false;
    if ( this.subs ) {
      this.subs.unsubscribe();
    }
  }

  refresh() {
    this.getPlate();
  }

  covertData(timestamp) {
    var date = new Date(timestamp*1000);
    var iso = date.toISOString().match(/(\d{2}:\d{2}:\d{2})/)
    return (iso[1]);
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'URL Base',
      subTitle: 'Alterar a URL Base do servidor',
      inputs: [
        {
          name: 'URL',
          placeholder: this.urlBase
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            this.urlBase = data.URL;
          }
        }
      ]
    });
    alert.present();
  }

  ver(ix) {
    const modal = this.modalCtrl.create('visualizar', {imagem: this.capturas[ix]} );
    modal.present();  
  }

}
